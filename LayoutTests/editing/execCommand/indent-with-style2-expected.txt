This tests indenting paragraphs with different inline styles combinations.
Bug 32233 Radar 7442387

one
two three
fourfive
four
foobar
Before: one <div id="test1"><span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">two</span> three</div> <div id="test2">four<span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">five</span></div> <div><span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">four</span></div> <ul><li><b>foo</b>bar</li></ul>
After: <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;"> one <div id="test1"><span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">two</span> three</div><div id="test2">four<span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">five</span></div><div><span class="Apple-style-span" style="background-color: rgb(255, 0, 0);">four</span></div></blockquote> <ul><ul><li><b>foo</b>bar</li></ul></ul>
