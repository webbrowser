KRenderingPaintServer {id="gradient" [type=LINEAR-GRADIENT] [stops=[(0.00,#FFFFFF), (1.00,#0000FF)]] [start=(0,0)] [end=(1,1)]}
KCanvasResource {id="filter_1" [type=FILTER]  [bounding box=at (-50.00%,-50.00%) size 200.00%x200.00%] [effect bounding box mode=1]}
KCanvasResource {id="filter_2" [type=FILTER]  [bounding box=at (0.00%,0.00%) size 200.00%x200.00%] [effect bounding box mode=1]}
KCanvasResource {id="filter_3" [type=FILTER]  [bounding box=at (-25.00%,-25.00%) size 200.00%x200.00%] [effect bounding box mode=1]}
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderSVGRoot {svg} at (10,10) size 372x100
    RenderSVGHiddenContainer {defs} at (0,0) size 0x0
      RenderSVGHiddenContainer {linearGradient} at (0,0) size 0x0
        RenderSVGGradientStop {stop} at (0,0) size 0x0
        RenderSVGGradientStop {stop} at (0,0) size 0x0
    RenderSVGContainer {g} at (10,10) size 372x100
      RenderPath {rect} at (10,10) size 100x100 [fill={[type=LINEAR-GRADIENT] [stops=[(0.00,#FFFFFF), (1.00,#0000FF)]] [start=(0,0)] [end=(1,1)]}] [filter=filter_1] [data="M10.00,10.00 L110.00,10.00 L110.00,110.00 L10.00,110.00 Z"]
      RenderPath {rect} at (160,10) size 50x50 [fill={[type=LINEAR-GRADIENT] [stops=[(0.00,#FFFFFF), (1.00,#0000FF)]] [start=(0,0)] [end=(1,1)]}] [filter=filter_2] [data="M160.00,10.00 L210.00,10.00 L210.00,60.00 L160.00,60.00 Z"]
      RenderPath {rect} at (332,22) size 50x50 [fill={[type=LINEAR-GRADIENT] [stops=[(0.00,#FFFFFF), (1.00,#0000FF)]] [start=(0,0)] [end=(1,1)]}] [filter=filter_3] [data="M332.00,22.00 L382.00,22.00 L382.00,72.00 L332.00,72.00 Z"]
