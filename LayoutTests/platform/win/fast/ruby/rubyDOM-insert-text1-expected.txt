layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x21
        RenderText {#text} at (0,0) size 656x20
          text run at (0,0) width 656: "The following is a test for DOM manipulation within <ruby>: Inserting a new text before a <rt> element"
      RenderBlock {P} at (0,37) size 784x21
        RenderText {#text} at (0,0) size 436x20
          text run at (0,0) width 436: "Both lines should look identical (the first line is the one manipulated)."
      RenderBlock (anonymous) at (0,74) size 784x42
        RenderBR {BR} at (0,0) size 0x20
        RenderBR {BR} at (0,21) size 0x20
      RenderBlock {P} at (0,132) size 784x34
        RenderText {#text} at (0,13) size 64x20
          text run at (0,13) width 64: "\x{6E96}\x{592A}\x{90CE}\x{306F}"
        RenderRuby (inline) {RUBY} at (0,0) size 109x20
          RenderRubyRun (anonymous) at (64,0) size 37x34
            RenderRubyText {RT} at (0,0) size 37x13
              RenderText {#text} at (0,0) size 37x13
                text run at (0,0) width 37: "\x{3068}\x{3046}\x{304D}\x{3087}\x{3046}"
            RenderRubyBase (anonymous) at (0,13) size 37x21
              RenderInline {SPAN} at (0,0) size 32x20
                RenderText {#text} at (2,0) size 32x20
                  text run at (2,0) width 32: "\x{6771}\x{4EAC}"
          RenderRubyRun (anonymous) at (101,0) size 38x34
            RenderRubyText {RT} at (0,0) size 38x13
              RenderText {#text} at (0,0) size 38x13
                text run at (0,0) width 38: "\x{3053}\x{3046}\x{304E}\x{3087}\x{3046}"
            RenderRubyBase (anonymous) at (0,13) size 38x21
              RenderText {#text} at (3,0) size 32x20
                text run at (3,0) width 32: "\x{5DE5}\x{696D}"
          RenderRubyRun (anonymous) at (139,0) size 34x34
            RenderRubyText {RT} at (0,0) size 34x13
              RenderText {#text} at (0,0) size 34x13
                text run at (0,0) width 34: "\x{3060}\x{3044}\x{304C}\x{304F}"
            RenderRubyBase (anonymous) at (0,13) size 34x21
              RenderInline {SPAN} at (0,0) size 32x20
                RenderText {#text} at (1,0) size 32x20
                  text run at (1,0) width 32: "\x{5927}\x{5B66}"
        RenderText {#text} at (173,13) size 125x20
          text run at (173,13) width 125: "\x{304B}\x{3089}\x{5352}\x{696D}\x{3057}\x{307E}\x{3057}\x{305F}\x{3002}"
      RenderBlock {P} at (0,182) size 784x34
        RenderText {#text} at (0,13) size 64x20
          text run at (0,13) width 64: "\x{6E96}\x{592A}\x{90CE}\x{306F}"
        RenderRuby (inline) {RUBY} at (0,0) size 109x20
          RenderRubyRun (anonymous) at (64,0) size 37x34
            RenderRubyText {RT} at (0,0) size 37x13
              RenderText {#text} at (0,0) size 37x13
                text run at (0,0) width 37: "\x{3068}\x{3046}\x{304D}\x{3087}\x{3046}"
            RenderRubyBase (anonymous) at (0,13) size 37x21
              RenderInline {SPAN} at (0,0) size 32x20
                RenderText {#text} at (2,0) size 32x20
                  text run at (2,0) width 32: "\x{6771}\x{4EAC}"
          RenderRubyRun (anonymous) at (101,0) size 38x34
            RenderRubyText {RT} at (0,0) size 38x13
              RenderText {#text} at (0,0) size 38x13
                text run at (0,0) width 38: "\x{3053}\x{3046}\x{304E}\x{3087}\x{3046}"
            RenderRubyBase (anonymous) at (0,13) size 38x21
              RenderText {#text} at (3,0) size 32x20
                text run at (3,0) width 32: "\x{5DE5}\x{696D}"
          RenderRubyRun (anonymous) at (139,0) size 34x34
            RenderRubyText {RT} at (0,0) size 34x13
              RenderText {#text} at (0,0) size 34x13
                text run at (0,0) width 34: "\x{3060}\x{3044}\x{304C}\x{304F}"
            RenderRubyBase (anonymous) at (0,13) size 34x21
              RenderInline {SPAN} at (0,0) size 32x20
                RenderText {#text} at (1,0) size 32x20
                  text run at (1,0) width 32: "\x{5927}\x{5B66}"
        RenderText {#text} at (173,13) size 125x20
          text run at (173,13) width 125: "\x{304B}\x{3089}\x{5352}\x{696D}\x{3057}\x{307E}\x{3057}\x{305F}\x{3002}"
